import React, { Component } from "react";
import { StyleSheet, Text, View, YellowBox } from "react-native";
import { createStackNavigator } from "react-navigation";
import { LoginScreen } from "./src/screens/login";
import { RegisterScreen } from "./src/screens/register";
import { ListScreen } from "./src/screens/list";
import { OptionsScreen } from "./src/screens/options";

YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);

const Navigator = createStackNavigator(
  {
    LoginScreen: {
      screen: LoginScreen
    },
    // OptionsScreen: { screen: OptionsScreen },
    // RegisterScreen: { screen: RegisterScreen },
    ListScreen: { screen: ListScreen }
  },

  {
    initialRouteName: "LoginScreen"
  }
);
export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        <Navigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
