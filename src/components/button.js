// import { Button } from "react-native-elements";
import styles from "./../style";
import React, { Fragment } from "react";
import { TouchableOpacity, Text } from "react-native";
// import Loading from "./loading";
export default (MyButton = props => {
  const {
    onPress = () => {},
    title = "title",
    disabled = false,
    style = {},
    styleTitle = {},
    loading = false
  } = props;
  return (
    <Fragment>
      <TouchableOpacity
        style={[styles.buttonStyle, style]}
        onPress={disabled ? () => {} : onPress}
        activeOpacity={0.5}
      >
        {/* {loading ? (
          <Loading />
        ) : ( */}
        <Text style={[styles.fontOpenSans18, styleTitle, { color: "white" }]}>
          {title}
        </Text>
        {/* )} */}
      </TouchableOpacity>
    </Fragment>
  );
});
