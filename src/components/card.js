// import { Button } from "react-native-elements";
import styles from "./../style";
import React, { Fragment } from "react";
import { View, Text } from "react-native";
// import Loading from "./loading";
export default (Card = props => {
  const {
    // onPress = () => {},
    name = "-",
    model = false,
    contact = null,
    jobNum = null,
    vehicleNumb = null,
    regNumb = null,
    inTime = null
  } = props.item;
  return (
    <Fragment>
      <View style={styles.cardCover}>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>
            {"Reg Number -" + regNumb}
          </Text>
        </View>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>
            {"Vehicle Number -" + vehicleNumb}
          </Text>
        </View>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>
            {"Customer Name -" + name}
          </Text>
        </View>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>
            {"Contact Number -" + contact}
          </Text>
        </View>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>{"In Time -" + inTime}</Text>
        </View>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>{"Job Id -" + jobNum}</Text>
        </View>
        <View style={styles.info}>
          <Text style={{ marginHorizontal: 10 }}>{"Model -" + model}</Text>
        </View>
      </View>
    </Fragment>
  );
});
