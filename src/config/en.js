const en = {
  Send_otp: "",
  valid_phone: "Please Enter a valid Mobile number",
  empty_phone: "Phone Number cannot be empty",
  valid_OTP: "Please Enter a valid OTP",
  empty_OTP: "OTP cannot be empty",
  send_OTP: "SEND OTP",
  login: "LOGIN",
  continue: "CONTINUE",
  no_policy: "No policy is registered with this mobile number"
};
export default en;
