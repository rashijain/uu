import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Dimensions,
  FlatList
} from "react-native";
import Base from "./listBase";
import TextInput from "../../components/textInput";
import strings from "../../config/en";
import styles from "../../style";
import Button from "../../components/button";
import Card from "../../components/card";
const item = {
  jobNum: "123",
  inTime: "123",
  name: "rashi",
  regNumb: "321",
  model: "1000/10",
  vehicleNumb: "test",
  contact: "8950079534"
};
export default class ListScreen extends Base {
  static navigationOptions = {
    title: "UW",
    headerStyle: {
      backgroundColor: "#FF6347"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "light"
    }
  };
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardShouldPersistTaps="always"
      >
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          contentContainerStyle={{
            flexDirection: "column",
            // flex: 1,
            justifyContent: "space-evenly"
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center"
              //   justifyContent: "space-evenly"
            }}
          >
            <TextInput
              labelColor="black"
              value={this.state.search}
              maxLength={15}
              capitalize="none"
              formInputWrapper={styles.searchFormInputWrapper}
              keyboardType={"numeric"}
              placeholder="Search By name,registration number,contact"
              onChangeText={text => {
                this.onChangeText("search", text);
              }}
            />

            <FlatList
              onEndReached={() => {
                this.getNewListOnScroll();
              }}
              style={{
                width: "100%"
              }}
              data={this.state.list}
              renderItem={({ item }) => {
                return <Card item={item} />;
              }}
              keyExtractor={(_, index) => index.toString()}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
