import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Dimensions
} from "react-native";
import TextInput from "../../components/textInput";
import strings from "../../config/en";
import styles from "../../style";
import Icons from "react-native-vector-icons/MaterialCommunityIcons";

import Button from "../../components/button";
const routes = [
  { title: "Vehicle Entry", routeName: "RegisterScreen" },
  { title: "Search List", routeName: "ListScreen" }
];

export default class OptionScreen extends Component {
  static navigationOptions = {
    title: "UW",
    headerLeft: null,
    headerStyle: {
      backgroundColor: "#FF6347"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "light"
    }
  };
  render() {
    return (
      <View style={styles.container}>
        {routes.map(item => {
          return (
            <TouchableOpacity
              key={item.name}
              onPress={() => {
                this.props.navigation.navigate(item.routeName);
              }}
              style={{
                height: 70,
                width: "100%",
                borderBottomWidth: 2,
                alignItems: "center",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <Text style={{ marginLeft: 20, fontSize: 18 }}>{item.title}</Text>
              <Icons
                name="chevron-right"
                size={25}
                color="black"
                style={{ marginRight: 20 }}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}
