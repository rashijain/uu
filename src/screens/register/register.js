import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Dimensions
} from "react-native";
import Base from "./registerBase";
import TextInput from "../../components/textInput";
import strings from "../../config/en";
import styles from "../../style";
import Button from "../../components/button";
export default class Register extends Base {
  static navigationOptions = {
    title: "UW",
    headerStyle: {
      backgroundColor: "#FF6347"
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "light"
    }
  };
  render() {
    let {
      jobNum,
      inTime,
      name,
      regNum,
      model,
      vehicleNum,
      contact
    } = this.state;
    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardShouldPersistTaps="always"
      >
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          contentContainerStyle={{
            flexDirection: "column"
            // flex: 1
            // justifyContent: "space-evenly"
          }}
        >
          <View
            style={{
              borderWidth: 1,
              margin: 20,
              flex: 1,
              alignItems: "center",
              justifyContent: "space-evenly"
            }}
          >
            <Text style={{ fontSize: 30, margin: 10 }}>Vehicle Entry</Text>
            <View>
              <TextInput
                labelColor="black"
                value={jobNum}
                formInputWrapper={styles.registerFormInputWrapper}
                maxLength={15}
                capitalize="none"
                keyboardType={"numeric"}
                placeholder="Job Number"
                onChangeText={text => {
                  this.onChangeText("jobNum", text);
                }}
              />
              <TextInput
                labelColor="black"
                value={inTime}
                maxLength={15}
                formInputWrapper={styles.registerFormInputWrapper}
                capitalize="none"
                keyboardType={"numeric"}
                placeholder="In Time"
                onChangeText={text => {
                  this.onChangeText("inTime", text);
                }}
              />
              <TextInput
                labelColor="black"
                value={name}
                maxLength={15}
                capitalize="none"
                formInputWrapper={styles.registerFormInputWrapper}
                keyboardType={"numeric"}
                placeholder="Customer Name"
                onChangeText={text => {
                  this.onChangeText("name", text);
                }}
              />
              <TextInput
                labelColor="black"
                value={regNum}
                maxLength={15}
                capitalize="none"
                formInputWrapper={styles.registerFormInputWrapper}
                keyboardType={"numeric"}
                placeholder="Registration Number"
                onChangeText={text => {
                  this.onChangeText("regNum", text);
                }}
              />
              <TextInput
                labelColor="black"
                value={model}
                maxLength={15}
                capitalize="none"
                formInputWrapper={styles.registerFormInputWrapper}
                keyboardType={"numeric"}
                placeholder="Model"
                onChangeText={text => {
                  this.onChangeText("model", text);
                }}
              />
              <TextInput
                labelColor="black"
                value={vehicleNum}
                maxLength={15}
                capitalize="none"
                formInputWrapper={styles.registerFormInputWrapper}
                keyboardType={"numeric"}
                placeholder="Vehicle Number"
                onChangeText={text => {
                  this.onChangeText("vehicleNum", text);
                }}
              />

              <TextInput
                labelColor="black"
                value={contact}
                maxLength={15}
                capitalize="none"
                formInputWrapper={styles.registerFormInputWrapper}
                keyboardType={"numeric"}
                placeholder="Contact Number"
                onChangeText={text => {
                  this.onChangeText("contact", text);
                }}
              />
            </View>
            <Button
              style={styles.registerButtonStyle}
              title={"Add"}
              loading={false}
              disableButton={false}
              onPress={this.add}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
