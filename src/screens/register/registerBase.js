import React, { Component } from "react";
import { Alert, Keyboard } from "react-native";
import { callWebService } from "../../utilities/serverApi";
import strings from "../../config/en";
import configFile from "../../config/config";
import { isEmpty } from "lodash";
export default class LoginBase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jobNum: "",
      inTime: "",
      name: "",
      regNum: "",
      model: "",
      vehicleNum: "",
      contact: ""
    };
  }
  checkEmpty = item => {
    if (item.trim() === "") {
      return true;
    } else return false;
  };
  showAlert = message => {
    Alert.alert("", message, [{ text: "OK", onPress: null }]);
  };
  validate = () => {
    let contact = this.state.contact.trim();
    let contact_flag = this.checkEmpty(this.state.contact);
    let name_flag = this.checkEmpty(this.state.name);
    let job_flag = this.checkEmpty(this.state.jobNum);
    let time_flag = this.checkEmpty(this.state.inTime);
    let reg_flag = this.checkEmpty(this.state.regNum);
    let model_flag = this.checkEmpty(this.state.model);
    let vehicle_flag = this.checkEmpty(this.state.vehicleNum);

    if (
      contact_flag ||
      name_flag ||
      job_flag ||
      time_flag ||
      reg_flag ||
      model_flag ||
      vehicle_flag
    ) {
      this.showAlert("All Fields are Mandatory");
      return false;
    }
    if (contact.length < 10 || !contact.match(/^\d+$/)) {
      this.showAlert(strings.valid_phone);
      this.setState({ contact: "" });
      return false;
    }
    return true;
  };
  add = () => {
    Keyboard.dismiss();
    if (this.validate()) {
      // const options = {
      //   method: "get",
      //   headers: {
      //     "Content-Type": "application/json"
      //   }
      // };
      // let url = `${configFile.SERVER_URL}/mobilenumber?mobile_number=${
      //   this.state.phone
      // }`;
      // fetch(url, options)
      //   .then(res => res.json())
      //   .then(parsedResponse => {
      //     if (isEmpty(parsedResponse.policy)) {
      //       this.showAlert(strings.no_policy);
      //       return;
      //     }
      //     this.props.navigation.navigate("OTPScreen", {
      //       contact_number: parsedResponse.policy[0].contact_number
      //     });
      //   })
      //   .catch(err => {
      //     this.showAlert("There is some Error !!");
      //     console.log("error is", err);
      //   });
    }
  };

  onChangeText = (field, value) => {
    this.state[field] = value;
    this.setState({});
  };
}
