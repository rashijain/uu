import { StyleSheet } from "react-native";
import { isIphoneX } from "./utilities/common";

export default StyleSheet.create({
  container: { flex: 1, backgroundColor: "white" },
  buttonStyle: {
    height: 45,
    width: 203,
    // borderRadius: 10,
    borderWidth: 1,
    borderColor: "lightgrey",
    backgroundColor: "#FF6347",
    alignItems: "center",
    justifyContent: "center"
  },
  errorMessageTextStyle: {
    color: "red",
    // fontFamily: "OpenSans-Light",
    fontSize: 12
    // lineHeight:815
  },
  errorMessageContainerStyle: {
    alignSelf: "flex-start",
    height: 18,
    paddingLeft: 10
  },
  registerlabel: {
    color: "black",
    alignSelf: "center",
    // fontFamily: "OpenSans",
    fontSize: 15,
    fontWeight: "500",
    marginVertical: 6
  },
  formInputText: {
    color: "#000000",
    fontSize: 16,
    fontFamily: "OpenSans",
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 10,
    paddingRight: 10
  },
  registerText: {
    fontSize: 16,
    // fontFamily: "OpenSans",
    // fontWeight: "300",
    color: "#646464",
    // marginTop: 7,
    alignSelf: "center"
  },
  formInputWrapper: {
    width: 263,
    height: 45,
    borderRadius: 0,
    // paddingVertical: -2,
    borderWidth: 1,
    // backgroundColor: "pink",
    // backgroundColor: "",
    paddingLeft: 14
  },
  searchFormInputWrapper: {
    width: 365,
    // marginHorizontal: 0,
    height: 50,
    borderRadius: 25,
    borderWidth: 2,
    borderColor: "lightgrey",
    // backgroundColor: "pink",
    // backgroundColor: "",
    paddingLeft: 14
  },
  registerFormInputWrapper: {
    width: 320,
    // marginHorizontal: 0,
    height: 36,
    borderRadius: 0,
    borderWidth: 1,
    // backgroundColor: "pink",
    // backgroundColor: "",
    paddingLeft: 10
  },
  info: {
    height: 30,
    width: "90%",
    marginTop: 2,
    justifyContent: "space-evenly"
  },
  cardCover: {
    // height: 150,
    width: "90%",
    marginVertical: 20,
    backgroundColor: "white",
    borderRadius: 10,
    elevation: 6,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "space-evenly",
    shadowColor: "rgba(0,0,0,50)",
    shadowOffset: {
      x: 0,
      y: 4
    }
  },
  registerButtonStyle: {
    height: 45,
    width: 320,
    margin: 20,
    borderRadius: 0,
    borderWidth: 1,

    backgroundColor: "#FF6347",
    alignItems: "center",
    justifyContent: "center"
  }
  // chatContainerStyle: {
  //   marginVertical: 10,
  //   marginRight: 20,
  //   marginLeft: 20,
  //   paddingHorizontal: 15,
  //   paddingTop: 15,
  //   paddingBottom: 10,
  //   borderRadius: 5,
  //   maxWidth: "75%"
  // },
  // chatTimeStyle: {
  //   alignSelf: "flex-end",
  //   fontSize: 10,
  //   color: "#7D7D7D",
  //   // fontFamily: "Roboto-Light",
  //   marginLeft: 30
  // },
  // chatTextStyle: {
  //   fontSize: 16,
  //   color: "black"
  //   // fontFamily: "Roboto-Light"
  // }
});
