import React from "react";
import { Dimensions, AsyncStorage, Platform } from "react-native";
// import DeviceInfo from "react-native-device-info";

let { height, width } = Dimensions.get("window");

// export function isIphoneX() {
//   return DeviceInfo.getModel() === "iPhone X";
// }
export function isIos() {
  return Platform.OS === "ios";
}

export { height, width };
